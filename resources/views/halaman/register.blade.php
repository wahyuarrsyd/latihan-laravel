<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcomee" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="nama_depan"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="nama_belakang"> <br><br>
        <label>Gender</label> <br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality:</label> <br><br>
        <select>
          <option>Indonesia</option>
          <option>Amerika</option>
          <option>Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio</label> <br><br>
        <textarea rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign up">
    </form>
</body>
</html>