<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $name1 = $request->nama_depan;
        $name2 = $request->nama_belakang;

        return view ('halaman.welcomee', compact('name1','name2'));
       
    }
    
}
